package net.boddin.clouddemo.dto;

import org.springframework.beans.factory.annotation.Required;

public class ExampleDto {
    private String name;

    public ExampleDto() {
    }

    public ExampleDto(String name) {
        this.name = name;
    }

    @Required
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
