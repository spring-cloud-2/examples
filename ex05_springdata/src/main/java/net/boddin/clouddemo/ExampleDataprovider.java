package net.boddin.clouddemo;

import net.boddin.clouddemo.entity.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.stereotype.Component;

@Component()
@ConditionalOnMissingClass("org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager")
public class ExampleDataprovider implements ApplicationRunner {

    private final ContactRepository repository;

    @Autowired
    public ExampleDataprovider(ContactRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(ApplicationArguments applicationArguments) {
        repository.save(new Contact("John", "Wick"));
        repository.save(new Contact("Mister", "Miagi"));
        repository.save(new Contact("Tyler", "Durden"));
        repository.save(new Contact("Denny", "Crane"));
        repository.save(new Contact("Sheldon", "Cooper"));
    }
}
