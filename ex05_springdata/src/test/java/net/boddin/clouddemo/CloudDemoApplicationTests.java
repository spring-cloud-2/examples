package net.boddin.clouddemo;

import net.boddin.clouddemo.entity.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = CloudDemoApplication.class)
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@AutoConfigureMockMvc
public class CloudDemoApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	private ContactController contactController;

	@MockBean
	private ContactRepository contactRepositoryMock;

	@Test
	public void contextLoads() {
	}

	@Test
	public void controllerGetAllWithMockTest() throws Exception {

		ArrayList<Contact> list = new ArrayList<>();
		list.add(new Contact("mr", "robot"));

		Mockito.when(contactRepositoryMock.findAll()).thenReturn(list);

		mvc.perform(get("/contacts"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$").isNotEmpty());
	}


}
