package net.boddin.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class Userprovider implements ApplicationRunner {

    private static Logger logger = LoggerFactory.getLogger(Userprovider.class);

    @Autowired
    private SecureUserRepository secureUserRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MyConfig myConfig;

    @Value("${a.property}")
    private String test;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.error("myconfig: " + myConfig.getTest());
        logger.error("a.property: " + test);
        String pw = passwordEncoder.encode("user");
        SecureUser us = new SecureUser("user", pw);
        secureUserRepository.save(us);
    }
}
