package net.boddin.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

@Transactional()
public interface SecureUserRepository extends CrudRepository<SecureUser, Long> {

    SecureUser findByUserName(String username);

}
